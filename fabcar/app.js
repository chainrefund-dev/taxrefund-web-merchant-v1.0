var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var session = require('express-session');




var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var member = require('./routes/member');
var main = require('./routes/main');
var tax = require('./routes/tax');
var pos = require('./routes/pos');
var merchant = require('./routes/merchant');
var product = require('./routes/product');
var transaction = require('./routes/transaction');
var confirm = require('./routes/confirm');

var app = express();
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use('assets/',express.static(path.join(__dirname,'assets/')));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({
    key: 'sid',
    secret: 'secret',
    resave: false,
    saveUninitialized: true,
    cookie: {
        maxAge: 24000 * 60 * 60 // 쿠키 유효기간 24시간
    }
}));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/member', member);
app.use('/main', main);
app.use('/tax', tax);
app.use('/pos', pos);
app.use('/merchant', merchant);
app.use('/product', product);
app.use('/transaction', transaction);
app.use('/confirm', confirm);



// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
