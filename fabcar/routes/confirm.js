var express = require('express');
var router = express.Router();
var session = require('express-session');
var session_user = null;
var app = express();
app.use(session({
    secret: '12312dajfj23rj2po4$#%@#',
    resave: false,
    saveUninitialized: true

}));
var user_id = null;
var role_type = null;
//db설정
var db = require('./sql.js');
var sysdb = db.sysConnect();
var posdb = db.posConnect();
//db설정 end
function getTimeStamp() {
    var d = new Date();

    var s =
        leadingZeros(d.getFullYear(), 4) + '-' +
        leadingZeros(d.getMonth() + 1, 2) + '-' +
        leadingZeros(d.getDate(), 2) + ' ' +

        leadingZeros(d.getHours(), 2) + ':' +
        leadingZeros(d.getMinutes(), 2) + ':' +
        leadingZeros(d.getSeconds(), 2);

    return s;
    // var xmlHttp;
    // //분기하지 않으면 IE에서만 작동된다.
    // if (window.XMLHttpRequest) { // IE 7.0 이상, 크롬, 파이어폭스일 경우 분기
    //     xmlHttp = new XMLHttpRequest();
    //     xmlHttp.open('HEAD',window.location.href.toString(),false);
    //     xmlHttp.setRequestHeader("Content-Type", "text/html");
    //     xmlHttp.send('');
    // }else if (window.ActiveXObject) {
    //     xmlHttp = new ActiveXObject('Msxml2.XMLHTTP');
    //     xmlHttp.open('HEAD',window.location.href.toString(),false);
    //     xmlHttp.setRequestHeader("Content-Type", "text/html");
    //     xmlHttp.send('');
    // }
    // var st = xmlHttp.getResponseHeader("Date");
    // var curDate = new Date(st);
    // var curDateFmt; var year = curDate.getFullYear();
    // var month = curDate.getMonth()+1;
    // var day = curDate.getDate();
    // var hours = curDate.getHours();
    // var minutes = curDate.getMinutes();
    // var second = curDate.getSeconds();
    // if(parseInt(month) < 10){
    //     month = 0 + "" + month;
    // }
    // if(parseInt(day) < 10){
    //     day = 0 + "" + day;
    // }
    // if(parseInt(hours) < 10){
    //     hours = 0 + "" + hours;
    // } if(parseInt(minutes) < 10){
    //     minutes = 0 + "" + minutes;
    // }if(parseInt(second) < 10){
    //     second = 0 + "" + second;
    // }
    //
    // curDateFmt = (year + "-" + month + "-" + day + " " + hours + ":" + minutes +":" + second);
    // return curDateFmt;
}
function leadingZeros(n, digits) {
    var zero = '';
    n = n.toString();

    if (n.length < digits) {
        for (i = 0; i < digits - n.length; i++)
            zero += '0';
    }
    return zero + n;
}

/* GET home page. */
router.get('/', function(req, res, next) {

    session_user =  req.session.usr_id

        res.render('confirm/confirm2', { title: 'confirm'  });


});
router.post('/goConfirm', function (req, res, next) {

    var Fabric_Client = require('fabric-client');
    var Fabric_CA_Client = require('fabric-ca-client');
    var path = require('path');
    var request = require('request');
    var fabric_client = new Fabric_Client();
    var store_path = path.join(__dirname, 'hfc-key-store');
    var user_id = null;
    var user_role = 'Client.store';
    var user_affiliation = null;
    var password = 'q1w2e3';

//ca server url
    var ca_server_url = 'https://52.78.211.97:7054';

    /**
     * Request Form example for sending to System Server
     */


//HTTP Request Api


//HTML 에서 받아오기
    user_id = req.body.id;
    password = req.body.pwd;


// create the key value store
    Fabric_Client.newDefaultKeyValueStore({
        path: store_path
    }).then((state_store) => {
        console.log('<<--------------------------------시작-------------------------------------->>');
// assign the store to the fabric client
        fabric_client.setStateStore(state_store);
        var crypto_suite = Fabric_Client.newCryptoSuite();
// use the same location for the state store (where the users' certificate are kept)
// and the crypto store (where the users' keys are kept)
        var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
        crypto_suite.setCryptoKeyStore(crypto_store);
        fabric_client.setCryptoSuite(crypto_suite);
        console.log('<<--------------------------------1-------------------------------------->>');
        var tlsOptions = {
            trustedRoots: [],
            verify: false
        };
// be sure to change the http to https when the CA is running TLS enabled
        return new Fabric_CA_Client(ca_server_url, tlsOptions, 'ca.chain-taxrefund.com', crypto_suite).enroll({
            enrollmentID: user_id,
            enrollmentSecret: password
        });

    }).then((enrollment) => {
        return fabric_client.createUser(
            {
                username: user_id,
                mspid: 'TaxRefundSysMSP',
                cryptoContent: {privateKeyPEM: enrollment.key.toBytes(), signedCertPEM: enrollment.certificate}
            });
    }).then((user) => {
        user.setRoles(user_role);
        user.setAffiliation(user_affiliation);
        return fabric_client.setUserContext(user);
    }).then(() => {
        console.log(user_id + '`s certificate was successfully set up with roles and affiliation');
        res.send({status : 200 , result : '인증서 발급 성공'})
    }).catch((err) => {
        console.error('Failed to register: ' + err);
        res.send({status : 500 , result : '인증서 발급 실패 아이디와 비밀번호를 확인하세요'})

    });





});





module.exports = router;
