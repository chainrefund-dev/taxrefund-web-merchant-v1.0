var express = require('express');
var router = express.Router();
var session = require('express-session');
var user_id = null;
var role_type = null;
//db설정
var db = require('./sql.js');
var posdb= db.posConnect();
var sysdb = db.sysConnect();
//db설정 end
    /* GET home page. */
router.get('/', function(req, res, next) {

     user_id =  req.session.user_id
     role_type =  req.session.role_type
    /*if(user_id != null){
        res.render('main', { title: 'main' ,user_id,role_type});
    }else{
        res.render('login', { title: 'login' ,err : '2' });
    }*/
    res.render('product/product', { title: 'product'});

});
router.post('/productList', function(req, res, next) {
    if(req.body.page !=null){
        var page = req.body.page;
    }else {
        var page = 1;
    }

    var startRow = (page-1) * 15;
    console.log(page);
    var params =[startRow, 15];

    var sql = 'select row_id ,prodNm, IneNo, prdCode, indPrice,indVat, indIct, indEdut, indStr, registered_date from store_product order by row_id desc limit ? ,? ';
    var sql2 = 'select * from store_product ';
    posdb.query(sql2, function (err, rows) {
        posdb.release
        if(err){
            console.log(err)
        } else {
            console.log("sql2처리"+rows.length)
            var tot = rows.length;
            posdb.query(sql,params, function (err, rows) {
                posdb.release
                if(err){
                    console.log(err)
                } else {
                    console.log("sql처리완료"+JSON.stringify(rows))
                    res.send({rows: rows , nowrow:tot});
                }
            });
        }
    });

});

router.post('/productList_one', function(req, res, next) {

    var sql = 'select row_id ,prodNm, IneNo, prdCode, indPrice,indVat, indIct, indEdut, indStr, registered_date from store_product where IneNo=? ';
    var no = req.body.IneNo
    console.log("no 확인"+no)
            posdb.query(sql,no, function (err, rows) {
                posdb.release
                if(err){
                    console.log(err)
                } else {
                    console.log("row보냄"+JSON.stringify(rows))
                    res.send({rows: rows});
                }
            });

});

router.get('/product_detail', function(req, res, next) {
    var no = req.query.no
    console.log("no: "+no)
    var sql = 'select row_id ,prodNm, IneNo, prdCode, indPrice,indVat, indIct, indEdut, indStr,date_format(registered_date, "%Y-%m-%d %T") as registered_date from store_product where row_id = ? ';


    posdb.query(sql,no, function (err, rows, fields) {
        posdb.release
        if(err){
            console.log(err)
        } else {


            res.render('product/product_detail', { rows: rows});
        }
    });


});


router.get('/product_add', function(req, res, next) {
    res.render('product/product_add', { title: 'product_add'});

});
router.post('/insertProduct', function(req, res, next) {
    console.log(req.body);
    var pronm = req.body.pronm;
    var price = req.body.price;
    var vat = req.body.vat;
    var con = 0;
    var edu = 0;
    var rular = 0;
    var params =[pronm,price,vat,con,edu,rular];

    var sql = 'insert into store_product (prodNm ,IneNo ,prdCode ,indPrice ,indVat ,indIct ,indEdut,indStr ,registered_date)';
    sql += 'values ( ? , concat("A",(select max(num) from (select ifnull((replace(IneNo,"A","")),0)+1 as num from store_product) cnt)) ,  concat("B",(select max(num) from (select ifnull((replace(prdCode,"B","")),0)+1 as num from store_product) cnt)) ,? ,? ,? ,? ,? ,now() )';


    posdb.query(sql,params, function (err, rows) {
        posdb.release
        if(err){
            console.log(err)
        } else {
            res.send({result: "success"});
        }


    });
});
router.post('/deleteProduct', function(req, res, next) {
    console.log(req.param('no'));

    var sql = 'delete from store_product where row_id = ?';


    posdb.query(sql,req.param('no'), function (err, rows) {
        posdb.release
        if(err){
            console.log(err)
        } else {
            res.send({result: "success"});
        }


    });
});


module.exports = router;
