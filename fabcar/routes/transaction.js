var express = require('express');
var router = express.Router();
var session = require('express-session');
var user_id = null;
var role_type = null;
const request = require('request');
//db설정
var db = require('./sql.js');
var posdb= db.posConnect();
var sysdb = db.sysConnect();
//db설정 end
function saleNum(n, width) {
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join('0') + n;
}
function getTimeStamp() {
    var d = new Date();

    var s =
        leadingZeros(d.getFullYear(), 4) + '-' +
        leadingZeros(d.getMonth() + 1, 2) + '-' +
        leadingZeros(d.getDate(), 2) + ' ' +

        leadingZeros(d.getHours(), 2) + ':' +
        leadingZeros(d.getMinutes(), 2) + ':' +
        leadingZeros(d.getSeconds(), 2);

    return s;

}
function getTimeStamp2() {
    var d = new Date();

    var s =
        leadingZeros(d.getFullYear(), 4) + '-' +
        leadingZeros(d.getMonth() + 1, 2) + '-' +
        leadingZeros(d.getDate(), 2) + ' ' +

        leadingZeros(d.getHours(), 2) + ':' +
        leadingZeros(d.getMinutes(), 2) + ':' +
        leadingZeros(d.getSeconds(), 2) + ':' +
        leadingZeros(d.getMilliseconds(), 3);

    return s;
}
function getTimeStampVal(val) {
    var d = new Date(val);

    var s =
        leadingZeros(d.getFullYear(), 4) + '-' +
        leadingZeros(d.getMonth() + 1, 2) + '-' +
        leadingZeros(d.getDate(), 2) + ' ' +

        leadingZeros(d.getHours(), 2) + ':' +
        leadingZeros(d.getMinutes(), 2) + ':' +
        leadingZeros(d.getSeconds(), 2);

    return s;

}
function leadingZeros(n, digits) {
    var zero = '';
    n = n.toString();

    if (n.length < digits) {
        for (i = 0; i < digits - n.length; i++)
            zero += '0';
    }
    return zero + n;
}
var generateRandom = function (min, max) {
    var ranNum = Math.floor(Math.random()*(max-min+1)) + min;
    return ranNum;
}
function goCustom(requestData, send ,date) {
    var headers = {
        'Content-Type': 'application/json'
    }
    request({
        // url: 'http://localhost:3003/custom/targetCheak2',
        url: 'http://www.chainsearch.info:3003/custom/targetCheak2',
        method: 'POST',
        headers: headers,
        form : {"result" : JSON.stringify(requestData)}

    },function(err, res, body) {
        var fine = getTimeStamp2()
        if(JSON.parse(body).result == 'success' ){
            send.send({status: 200, result: 'success',nDate : date ,pDate : JSON.parse(body).date, tDate :fine , transaction : JSON.parse(body).transaction});
        }else if(JSON.parse(body).result == 'limitExcess' ){
            send.send({status: 200, result: 'limitExcess',nDate : date ,pDate : JSON.parse(body).date, tDate :fine , transaction : JSON.parse(body).transaction});
        }else  if(JSON.parse(body).result == 'minLimitExcess' ){
            send.send({status: 200, result: 'minLimitExcess',nDate : date ,pDate : JSON.parse(body).date, tDate :fine , transaction : JSON.parse(body).transaction});
        }else{
            send.send({status: 500, result: 'err'});
        }


    });
}
    /* GET home page. */
router.get('/', function(req, res, next) {

     user_id =  req.session.user_id
     role_type =  req.session.role_type
    /*if(user_id != null){
        res.render('main', { title: 'main' ,user_id,role_type});
    }else{
        res.render('login', { title: 'login' ,err : '2' });
    }*/
    res.render('transaction/transaction', { title: 'transaction' });


});
router.post('/transactionList', function(req, res, next) {

    var page = req.body.page;
    var startRow = (page-1) * 15;
    console.log(page);
    var params =[startRow, 15];

    var sql = 'select * ,date_format(saleDatm, "%Y-%m-%d %T") as sal_reg_date, date_format(sal_tax_reg_date, "%Y-%m-%d %T") as tax_reg_date from store_sale order by row_id desc limit ? ,? ';
    var sql2 = 'select * from store_sale ';
    posdb.query(sql2, function (err, rows) {
        posdb.release
        if(err){
            console.log(err)
        } else {
            console.log("sql2처리"+rows.length)
            var tot = rows.length;
            posdb.query(sql,params, function (err, rows) {
                posdb.release
                if(err){
                    console.log(err)
                } else {
                    console.log("sql처리완료"+JSON.stringify(rows))
                    console.log("sql처리완료"+rows.details)
                    res.send({rows: rows , nowrow:tot});
                }
            });
        }
    });

});
router.get('/transaction_detail', function(req, res, next) {
    var no = req.query.no
    console.log("no: "+no)
    var sql = 'select *,date_format(saleDatm, "%Y-%m-%d %T") as sal_reg_date,date_format(sal_tax_reg_date, "%Y-%m-%d %T") as sal_tax_reg_date2 from store_sale ss left join store_info_private sip on ss.store_id =sip.store_id where ss.row_id = ? ';


    posdb.query(sql,no, function (err, rows, fields) {
        posdb.release
        if(err){
            console.log(err)
        } else {

            console.log("sql처리완료"+rows[0].tx_id);

            if(rows[0].tx_id != null){
                console.log("트랜잭션 리스트 시작")
                var dbtx_id =rows[0].tx_id
                var user_id = rows[0].store_id.toString();
                var sql2 = ' select transaction_info_private.row_id' +
                    '      ,transaction_info_private.reference_id as reference_id ' +
                    '      ,request_type' +
                    '      ,transaction_info_private.customer_id as customer_id' +
                    '      ,store_info_private.store_id as store_id' +
                    '      ,store_info_private.store_name as store_name' +
                    '      ,transaction_info_private.taxRefunder_id as taxRefunder_id' +
                    '      ,refund_info_private.taxRefunder_name' +
                    '      ,customer_id' +
                    '      ,purchsSn' +
                    '      ,totQty' +
                    '      ,totAmt' +
                    '      ,details' +
                    '      ,date_format(tx_created, "%Y-%m-%d %T") as tx_created' +
                    '      ,sal_tax_price' +
                    '      ,cur_tx_id' +
                    '      ,prev_tx_id' +
                    '      ,refund_type' +
                    '      ,tx_status' +
                    '      ,date_format(saleDatm, "%Y-%m-%d %T") as saleDatm '+
                    '           from transaction_info_private  ' +
                    '       left join store_info_private on transaction_info_private.store_id = store_info_private.store_id ' +
                    '       left join refund_info_private on transaction_info_private.taxRefunder_id = refund_info_private.taxRefunder_id ' +
                    '       where reference_id =? order by tx_created desc limit 1'

                sysdb.query(sql2,dbtx_id, function (err, rows2) {
                    sysdb.release
                    if (err) {
                        console.log(err)
                    } else {
                        console.log(rows2)
                        res.render('transaction/transaction_detail', {
                            rows: rows,
                            result : rows2
                        });
                    }
                });
            }else{
                res.render('transaction/transaction_detail', { rows: rows ,result : 'null'});
            }


        }
    });


});
router.post('/insertTransaction', function(req, res, next) {
    console.log(req.body);
    var totQty = req.body.utot;
    var totAmt = req.body.upsum;
    var store_id = req.body.sel_pos
    var proarr ="";
    var proarr2 ="";
    var proarr3 ="";
    var proarr4 ="";

    if(req.body.sel_pro !='' ){proarr='{"prodNm" : "'+req.body.upronm+'", "IneNo" : "'+req.body.sel_pro+'", "indQty" : "'+req.body.cnt+'", "indPrice" : "'+req.body.uprice+'", "prdCode" : "'+req.body.prdCode+'", "salePrice" : "'+req.body.uprice*req.body.cnt+'", "indVat" : "'+req.body.uvat+'", "indIct" : "'+req.body.indIct+'", "indEdut" : "'+req.body.indEdut+'", "indStr" : "'+req.body.indStr+'"},'}
    if(req.body.sel_pro2 !='' ){proarr2='{"prodNm" : "'+req.body.upronm2+'", "IneNo" : "'+req.body.sel_pro2+'", "indQty" : "'+req.body.cnt2+'", "indPrice" : "'+req.body.uprice2+'", "prdCode" : "'+req.body.prdCode2+'", "salePrice" : "'+req.body.uprice2*req.body.cnt2+'", "indVat" : "'+req.body.uvat2+'", "indIct" : "'+req.body.indIct2+'", "indEdut" : "'+req.body.indEdut2+'", "indStr" : "'+req.body.indStr2+'"},'}
    if(req.body.sel_pro3 !='' ){proarr3='{"prodNm" : "'+req.body.upronm3+'", "IneNo" : "'+req.body.sel_pro3+'", "indQty" : "'+req.body.cnt3+'", "indPrice" : "'+req.body.uprice3+'", "prdCode" : "'+req.body.prdCode3+'", "salePrice" : "'+req.body.uprice3*req.body.cnt3+'", "indVat" : "'+req.body.uvat3+'", "indIct" : "'+req.body.indIct3+'", "indEdut" : "'+req.body.indEdut3+'", "indStr" : "'+req.body.indStr3+'"},'}
    if(req.body.sel_pro4 !='' ){proarr4='{"prodNm" : "'+req.body.upronm4+'", "IneNo" : "'+req.body.sel_pro4+'", "indQty" : "'+req.body.cnt4+'", "indPrice" : "'+req.body.uprice4+'", "prdCode" : "'+req.body.prdCode4+'", "salePrice" : "'+req.body.uprice4*req.body.cnt4+'", "indVat" : "'+req.body.uvat4+'", "indIct" : "'+req.body.indIct4+'", "indEdut" : "'+req.body.indEdut4+'", "indStr" : "'+req.body.indStr4+'"},'}

    // if(req.body.sel_pro2 !='' ){proarr2=', {"prodNm" : "'+req.body.upronm2+'", "IneNo" : "'+req.body.sel_pro2+'", "indQty" : '+req.body.cnt2+', "indPrice" : '+req.body.uprice2+'}'}
    // if(req.body.sel_pro3 !='' ){proarr3=', {"prodNm" : "'+req.body.upronm3+'", "IneNo" : "'+req.body.sel_pro3+'", "indQty" : '+req.body.cnt3+', "indPrice" : '+req.body.uprice3+'}'}
    var details = "["+proarr+proarr2+proarr3+proarr4
    if(details !="["){
        details =details.slice(0,-1)
    }
    details +=']'
    var sal_tax_price = req.body.usvat;
    console.log(details);
    var sal_tax_type = "";
    if(req.body.radio3=='yes'){sal_tax_type = 0}else if(req.body.radio3=='no'){sal_tax_type = 1}


    var sql2 = "";
    if(sal_tax_type == 0){
        sql2 = 'insert into store_sale (' +
            'store_id ' +
            ',purchsSn ' +
            ',totQty ' +
            ',totAmt ' +
            ',details ' +
            ',saleDatm ' +
            ',sal_tax_price' +
            ',sal_tax_type ' +
            ',sal_tax_yn ' +
            ')';
        sql2 += 'values ( ?,?,?,?,?,date_format(now(), "%Y-%m-%d %T"), ?, ?, 0)';
    }

    var sql ='select ifnull(max(replace(purchsSn,"SA","")),0)+1 as num from store_sale'

    posdb.query(sql, function (err, rows) {
        posdb.release
        if(err){
            console.log(err)
        } else {
            var purchsSn = rows[0].num
            purchsSn = "SA"+saleNum(purchsSn,6);
            console.log("판매번호 생성 : "+purchsSn);


            var params = [store_id, purchsSn, totQty,totAmt,details,sal_tax_price,sal_tax_type];
            posdb.query(sql2 ,params, function (err, rows) {
                posdb.release
                if(err){
                    console.log(err)
                }else{
                    res.send({result: "success" ,purchsSn :purchsSn});
                }
            })

        }
    });
})
var cnt =0;
router.post('/insertTransaction2', function(req, res, next) {
    console.log(req.body);
    var totQty = req.body.utot;
    var totAmt = req.body.upsum;
    var store_id = req.body.sel_pos
    var proarr ="";
    var proarr2 ="";
    var proarr3 ="";
    var proarr4 ="";
    if(req.body.sel_pro !='' ){proarr='{"prodNm" : "'+req.body.upronm+'", "IneNo" : "'+req.body.sel_pro+'", "indQty" : "'+req.body.cnt+'", "indPrice" : "'+req.body.uprice+'", "prdCode" : "'+req.body.prdCode+'", "salePrice" : "'+req.body.uprice*req.body.cnt+'", "indVat" : "'+req.body.uvat+'", "indIct" : "'+req.body.indIct+'", "indEdut" : "'+req.body.indEdut+'", "indStr" : "'+req.body.indStr+'"},'}
    if(req.body.sel_pro2 !='' ){proarr2='{"prodNm" : "'+req.body.upronm2+'", "IneNo" : "'+req.body.sel_pro2+'", "indQty" : "'+req.body.cnt2+'", "indPrice" : "'+req.body.uprice2+'", "prdCode" : "'+req.body.prdCode2+'", "salePrice" : "'+req.body.uprice2*req.body.cnt2+'", "indVat" : "'+req.body.uvat2+'", "indIct" : "'+req.body.indIct2+'", "indEdut" : "'+req.body.indEdut2+'", "indStr" : "'+req.body.indStr2+'"},'}
    if(req.body.sel_pro3 !='' ){proarr3='{"prodNm" : "'+req.body.upronm3+'", "IneNo" : "'+req.body.sel_pro3+'", "indQty" : "'+req.body.cnt3+'", "indPrice" : "'+req.body.uprice3+'", "prdCode" : "'+req.body.prdCode3+'", "salePrice" : "'+req.body.uprice3*req.body.cnt3+'", "indVat" : "'+req.body.uvat3+'", "indIct" : "'+req.body.indIct3+'", "indEdut" : "'+req.body.indEdut3+'", "indStr" : "'+req.body.indStr3+'"},'}
    if(req.body.sel_pro4 !='' ){proarr4='{"prodNm" : "'+req.body.upronm4+'", "IneNo" : "'+req.body.sel_pro4+'", "indQty" : "'+req.body.cnt4+'", "indPrice" : "'+req.body.uprice4+'", "prdCode" : "'+req.body.prdCode4+'", "salePrice" : "'+req.body.uprice4*req.body.cnt4+'", "indVat" : "'+req.body.uvat4+'", "indIct" : "'+req.body.indIct4+'", "indEdut" : "'+req.body.indEdut4+'", "indStr" : "'+req.body.indStr4+'"},'}

    // if(req.body.sel_pro2 !='' ){proarr2=', {"prodNm" : "'+req.body.upronm2+'", "IneNo" : "'+req.body.sel_pro2+'", "indQty" : '+req.body.cnt2+', "indPrice" : '+req.body.uprice2+'}'}
    // if(req.body.sel_pro3 !='' ){proarr3=', {"prodNm" : "'+req.body.upronm3+'", "IneNo" : "'+req.body.sel_pro3+'", "indQty" : '+req.body.cnt3+', "indPrice" : '+req.body.uprice3+'}'}
    var details = "["+proarr+proarr2+proarr3+proarr4
    if(details !="["){
        details =details.slice(0,-1)
    }
    details +=']'
    var sal_tax_price = req.body.usvat;
    console.log(details);
    var sal_tax_type = "";
    if(req.body.radio3=='yes'){sal_tax_type = 0}else if(req.body.radio3=='no'){sal_tax_type = 1}
    var sql ='select ifnull(max(replace(purchsSn,"SA","")),0)+1 as num from store_sale'
    var purchsSn =''
    posdb.query(sql, function (err, rows) {
        posdb.release
        if(err){
            console.log(err)
        } else {
            purchsSn = rows[0].num


            var Fabric_Client = require('fabric-client');
            var Fabric_CA_Client = require('fabric-ca-client');
            var path = require('path');
            var util = require('util');
            var os = require('os');
            var fabric_client = new Fabric_Client();

// setup the fabric network
            var channel = fabric_client.newChannel('ch-taxrefund');
            var peer = fabric_client.newPeer('grpc://ec2-13-209-181-30.ap-northeast-2.compute.amazonaws.com:7051');
            // var peer = fabric_client.newPeer('grpc://192.168.0.28:7051');
            channel.addPeer(peer);
            var order = fabric_client.newOrderer('grpc://www.chainsearch.info:7050')
            // var order = fabric_client.newOrderer('grpc://192.168.0.28:7050')
            channel.addOrderer(order);

            var member_user = null;
            var store_path = path.join(__dirname, 'hfc-key-store');
            console.log('Store path:'+store_path);
            var fabric_ca_client = null;
            var tx_id = null;
            var request = {};
            var crypto_suite = null;

//User ID
            var client_id = 'CertAdmin'
            purchsSn = "SA"+saleNum(purchsSn,6);
            console.log("판매번호 생성 : "+purchsSn);
            var store_id = req.body.sel_pos
            var customer_id = req.body.user_id;
            var taxRefunder_id = ''
            var totAmt= ""
            var totQty= ""
            var totRefund= ""
            var totVat= ""
            var totEdut= 0
            var totIct= 0
            var totStr =0
            var object =[]
            var date =getTimeStamp().toString()
            var datemtime = getTimeStamp2()
            var dateTime = new Date().getTime()+""+leadingZeros(cnt,2)
            console.log("dateTime1 : "+ dateTime)
            if(cnt <99){
                cnt++
            }else{
                cnt =0
            }
            dateTime = dateTime.substring(1,15)
            console.log("dateTime2 : "+ dateTime)
            var tx_id2 =''
            var sql2 ='SELECT * FROM store_info_private where store_id = ?'
            sysdb.query(sql2,store_id, function(err, rows) {
                sysdb.release
                if(err){
                    console.log(err)
                }else {
                    console.log("sql2 실행")
                    taxRefunder_id = rows[0].taxrefunder_id
                    console.log("환급사 정보 : ", taxRefunder_id)
                }

            });
            var requestData= {
                header:
                    {
                        reference_id: "",
                        tx_status: "",
                        request_type: "",
                        refund_type: "",
                        cur_tx_id: "",
                        prev_tx_id: ""
                    },
                body: {
                    customer_id: "",
                    taxRefunder_id: "",
                    store_id: "",
                    tx_created: "",
                    purchsSn: "",
                    saleDatm: "",
                    totQty: "",
                    totAmt: "",
                    totRefund: "",
                    totVat: "",
                    totIct: "",
                    totEdut: "",
                    totStr: "",
                    details: ""

                }
            }


//amount for tax refund
            var amount = req.body.amount;

            var currentDate= new Date();
            var datetime = currentDate.getFullYear()+"-"
                +currentDate.getUTCMonth()+"-"+
                currentDate.getDate()+" "+
                currentDate.getHours()+":"+
                currentDate.getMinutes()+":"+
                currentDate.getSeconds();
            // let arrayObject = {};
            // arrayObject=JSON.stringify(rows[0].details);
            let str = details
            // str = str.slice(0,-2)
            // str = str.slice(2)
            object.push((str))
            // details =details.slice(0,-1)

            totAmt= req.body.upsum;
            totEdut= 0
            totIct= 0
            totStr= 0
            totQty= req.body.utot;
            totRefund= req.body.usvat
            totVat= req.body.usvat
            Fabric_Client.newDefaultKeyValueStore({ path: store_path
            }).then((state_store) => {
                // assign the store to the fabric client
                fabric_client.setStateStore(state_store);
                crypto_suite = Fabric_Client.newCryptoSuite();
                // use the same location for the state store (where the users' certificate are kept)
                // and the crypto store (where the users' keys are kept)
                var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
                crypto_suite.setCryptoKeyStore(crypto_store);
                fabric_client.setCryptoSuite(crypto_suite);
                fabric_ca_client = new Fabric_CA_Client('https://ec2-52-78-211-97.ap-northeast-2.compute.amazonaws.com:7054', null , '', crypto_suite);
                // get the enrolled user from persistence, this user will sign all requests
                return fabric_client.getUserContext(client_id, true);
            }).then((user_from_store) => {
                if (user_from_store && user_from_store.isEnrolled()) {
                    console.log('1. Successfully loaded "client" user - {client_id: '+client_id+'} from persistence');
                } else {
                    throw new Error('Failed to get "client user" - {client_id: ' +client_id+'} .... run registerUser.js');
                }


                return fabric_client.getUserContext('CertAdmin', true);
            }).then((admin_from_store) => {
                if (admin_from_store && admin_from_store.isEnrolled()) {
                    console.log('2. Successfully loaded {CertAdmin} from persistence - in order to access CA Server via CertAdmin certificate');
                } else {
                    throw new Error('Failed to get "CertAdmin".... run enrollAdmin.js');
                }
                // get a transaction id object based on the current user assigned to fabric client

                return fabric_ca_client.newIdentityService().getOne(store_id, admin_from_store);
            }).then((store_from_server) => {

                // next we need to enroll the user with CA server
                if (store_from_server.result.type === 'Client.store'){
                    member_user = store_from_server;
                }else{
                    throw new Error('Failed to get "Client.store" user.... check your store_id again');
                }
                console.log('3. Successfully loaded "Client.store" user membership info {store_id: '+store_id+', type:' + store_from_server.result.type+'}');

                tx_id = fabric_client.newTransactionID();
                console.log("4. Assigning transaction_id: ", tx_id.getTransactionID());
                // console.log(" rows[0].details.length: ", (rows[0].details).length);
                // console.log(" rows[0].details ",  JSON.parse(rows[0].details));
                console.log(" 오브젝트 : ",  rows[0].details);

                // var tx_id2 = tx_id.getTransactionID()


                tx_id2 = dateTime;
                requestData= {
                    header:
                        {
                            reference_id: tx_id2,
                            tx_status: "1",
                            request_type: "200",
                            refund_type: req.body.refund_type,
                            cur_tx_id: tx_id2,
                            prev_tx_id: ""
                        },
                    body: {
                        customer_id: customer_id,
                        taxRefunder_id: taxRefunder_id,
                        store_id: store_id,
                        tx_created: date,
                        purchsSn: purchsSn,
                        saleDatm: req.body.sDate_1,
                        totQty: totQty.toString(),
                        totAmt: totAmt.toString(),
                        totRefund: totRefund.toString(),
                        totVat: totVat.toString(),
                        totIct: totIct.toString(),
                        totEdut: totEdut.toString(),
                        totStr: totStr.toString(),
                        details: JSON.parse(details)


                    }
                }
                console.log(requestData)
                request = {
                    chaincodeId: 'cc-taxrefund',
                    chaincodeVersion: 'v1.0',
                    fcn: 'invokePending',
                    args: ['Client.store',JSON.stringify(requestData)],
                    chainId: 'ch-taxrefund',
                    txId: tx_id
                };

                console.log("5. Send Transaction Request - ", );
                // send the transaction proposal to the peers
                return channel.sendTransactionProposal(request);
            }).then((results) => {
                var proposalResponses = results[0];
                var proposal = results[1];
                let isProposalGood = false;
                if (proposalResponses && proposalResponses[0].response &&
                    proposalResponses[0].response.status === 200) {
                    isProposalGood = true;
                    // console.log('Transaction proposal was good');
                } else {
                    // console.error('Transaction proposal was bad');
                }
                if (isProposalGood) {
                    console.log(util.format(
                        '6. Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s"',
                        proposalResponses[0].response.status, proposalResponses[0].response.message));

                    // build up the request for the orderer to have the transaction committed
                    var request = {
                        proposalResponses: proposalResponses,
                        proposal: proposal
                    };

                    // set the transaction listener and set a timeout of 30 sec
                    // if the transaction did not get committed within the timeout period,
                    // report a TIMEOUT status
                    var transaction_id_string = tx_id.getTransactionID(); //Get the transaction ID string to be used by the event processing
                    var promises = [];

                    var sendPromise = channel.sendTransaction(request);
                    promises.push(sendPromise); //we want the send transaction first, so that we know where to check status

                    // get an eventhub once the fabric client has a user assigned. The user
                    // is required bacause the event registration must be signed
                    let event_hub = fabric_client.newEventHub();
                    event_hub.setPeerAddr('grpc://www.chainsearch.info:7053');
                    // event_hub.setPeerAddr('grpc://192.168.0.28:7053');

                    // using resolve the promise so that result status may be processed
                    // under the then clause rather than having the catch clause process
                    // the status
                    let txPromise = new Promise((resolve, reject) => {
                        let handle = setTimeout(() => {
                            event_hub.disconnect();
                            resolve({event_status : 'TIMEOUT'}); //we could use reject(new Error('Trnasaction did not complete within 30 seconds'));
                        }, 3000);
                        event_hub.connect();
                        event_hub.registerTxEvent(transaction_id_string, (tx, code) => {
                            // this is the callback for transaction event status
                            // first some clean up of event listener
                            clearTimeout(handle);
                            event_hub.unregisterTxEvent(transaction_id_string);
                            event_hub.disconnect();

                            // now let the application know what happened
                            var return_status = {event_status : code, tx_id : transaction_id_string};
                            if (code !== 'VALID') {
                                console.error('The transaction was invalid, code = ' + code);
                                resolve(return_status); // we could use reject(new Error('Problem with the tranaction, event status ::'+code));
                            } else {
                                console.log('7. The transaction has been committed on peer ' + event_hub._ep._endpoint.addr);
                                resolve(return_status);
                            }
                        }, (err) => {
                            //this is the callback if something goes wrong with the event registration or processing
                            reject(new Error('There was a problem with the eventhub ::'+err));
                        });
                    });
                    promises.push(txPromise);

                    return Promise.all(promises);
                } else {
                    console.error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
                    throw new Error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
                }
            }).then((results) => {
                // console.log('8. Send transaction completed: '+JSON.stringify(results));
                // console.log('8. Send transaction completed: '+results[1].tx_id);
                // var dbtx_id = results[1].tx_id
                var dbtx_id = tx_id2
                var params2 = [dbtx_id, date, purchsSn]

                var sql3 = 'update store_sale set tx_id = ?,sal_tax_yn = 1,sal_tax_reg_date = ? where purchsSn = ?'
                posdb.query(sql3, params2, function (err, rows) {
                    posdb.release
                    if (err) {
                        console.log(err)
                    } else {
                        console.log("sql tx_id 저장")
                    }
                });

                // check the results in the order the promises were added to the promise all list
                if (results && results[0] && results[0].status === 'SUCCESS') {
                    console.log('9. Successfully sent transaction to the orderer.');
                    if (results && results[1] && results[1].event_status === 'VALID') {
                        console.log('10. Successfully committed the change to the ledger by the peer');
                        //db 트랜잭션 저장 start
                        var insertsql = 'insert into transaction_info_private (store_id,taxRefunder_id,customer_id,purchsSn,totQty,totAmt,details,tx_created,sal_tax_price,cur_tx_id,reference_id,prev_tx_id,refund_type,request_type,tx_status,saleDatm )';
                        insertsql += 'values (  ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ,?)';
                        var params = [
                            store_id
                            , taxRefunder_id
                            , customer_id
                            , purchsSn
                            , totQty
                            , totAmt
                            , (details)
                            , date
                            , totRefund
                            , tx_id2
                            , tx_id2
                            , ""
                            , req.body.refund_type
                            , "200"
                            , "1"
                            , req.body.sDate_1];

                        sysdb.query(insertsql ,params, function (err, rows) {
                            sysdb.release
                            if(err){
                                console.log(err)
                            }else{
                                console.log('success')
                            }
                        })
                        //db 트랜잭션 저장 end
                        return goCustom(requestData, res,datemtime);
                    } else {
                        console.log('Transaction failed to be committed to the ledger due to ::' + results[1].event_status);
                    }
                } else {
                    console.error('Failed to order the transaction. Error code: ' + response.status);
                }


            }).then((tDate) => {
                // if (tDate == 'err'){
                //     console.log("----------------goCustom!!!!!!!!!!-=---------------------")
                //     res.send({result: 500, result: 'custom server error', date : date});
                // }else{
                //     console.log("----------------goCustom!!!!!!!!!!-=---------------------")
                //     res.send({status: 200, result: 'success',pDate : date, tDate : tDate});
                // }
            }).catch((err) => {
                console.error('Failed to invoke successfully :: ' + err);
                res.send({result: 500, result: 'store server error', date : date});
            });
        }
    });


})
router.post('/insertTransaction3', function(req, res, next) {
    console.log(req.body);
    var totQty = req.body.utot;
    var totAmt = req.body.upsum;
    var store_id = req.body.sel_pos
    var purchsSn = req.body.purchsSn_1
    var sDate = req.body.sDate_1
    var pDate = getTimeStampVal(req.body.pDate_1)
    var tDate = getTimeStampVal(req.body.tDate_1)
    var tx_id = req.body.tx_id
    var proarr ="";
    var proarr2 ="";
    var proarr3 ="";
    var proarr4 ="";

    if(req.body.sel_pro !='' ){proarr='{"prodNm" : "'+req.body.upronm+'", "IneNo" : "'+req.body.sel_pro+'", "indQty" : "'+req.body.cnt+'", "indPrice" : "'+req.body.uprice+'", "prdCode" : "'+req.body.prdCode+'", "salePrice" : "'+req.body.uprice*req.body.cnt+'", "indVat" : "'+req.body.uvat+'", "indIct" : "'+req.body.indIct+'", "indEdut" : "'+req.body.indEdut+'", "indStr" : "'+req.body.indStr+'"},'}
    if(req.body.sel_pro2 !='' ){proarr2='{"prodNm" : "'+req.body.upronm2+'", "IneNo" : "'+req.body.sel_pro2+'", "indQty" : "'+req.body.cnt2+'", "indPrice" : "'+req.body.uprice2+'", "prdCode" : "'+req.body.prdCode2+'", "salePrice" : "'+req.body.uprice2*req.body.cnt2+'", "indVat" : "'+req.body.uvat2+'", "indIct" : "'+req.body.indIct2+'", "indEdut" : "'+req.body.indEdut2+'", "indStr" : "'+req.body.indStr2+'"},'}
    if(req.body.sel_pro3 !='' ){proarr3='{"prodNm" : "'+req.body.upronm3+'", "IneNo" : "'+req.body.sel_pro3+'", "indQty" : "'+req.body.cnt3+'", "indPrice" : "'+req.body.uprice3+'", "prdCode" : "'+req.body.prdCode3+'", "salePrice" : "'+req.body.uprice3*req.body.cnt3+'", "indVat" : "'+req.body.uvat3+'", "indIct" : "'+req.body.indIct3+'", "indEdut" : "'+req.body.indEdut3+'", "indStr" : "'+req.body.indStr3+'"},'}
    if(req.body.sel_pro4 !='' ){proarr4='{"prodNm" : "'+req.body.upronm4+'", "IneNo" : "'+req.body.sel_pro4+'", "indQty" : "'+req.body.cnt4+'", "indPrice" : "'+req.body.uprice4+'", "prdCode" : "'+req.body.prdCode4+'", "salePrice" : "'+req.body.uprice4*req.body.cnt4+'", "indVat" : "'+req.body.uvat4+'", "indIct" : "'+req.body.indIct4+'", "indEdut" : "'+req.body.indEdut4+'", "indStr" : "'+req.body.indStr4+'"},'}

    // if(req.body.sel_pro2 !='' ){proarr2=', {"prodNm" : "'+req.body.upronm2+'", "IneNo" : "'+req.body.sel_pro2+'", "indQty" : '+req.body.cnt2+', "indPrice" : '+req.body.uprice2+'}'}
    // if(req.body.sel_pro3 !='' ){proarr3=', {"prodNm" : "'+req.body.upronm3+'", "IneNo" : "'+req.body.sel_pro3+'", "indQty" : '+req.body.cnt3+', "indPrice" : '+req.body.uprice3+'}'}
    var details = "["+proarr+proarr2+proarr3+proarr4
    if(details !="["){
        details =details.slice(0,-1)
    }
    details +=']'
    var sal_tax_price = req.body.usvat;
    console.log(details);
    var sal_tax_type = "";
    if(req.body.radio3=='yes'){sal_tax_type = 0}else if(req.body.radio3=='no'){sal_tax_type = 1}

    var params = [];
    var sql2 = "";
    if(sal_tax_type == 1){

        if(req.body.tx_status =="1"){
            sql2 = 'insert into store_sale (' +
                'store_id ' +
                ',purchsSn ' +
                ',totQty ' +
                ',totAmt ' +
                ',details ' +
                ',saleDatm ' +
                ',sal_tax_price' +
                ',sal_tax_type ' +
                ',sal_tax_yn ' +
                ',sal_tax_reg_date ' +
                ',sal_tax_fin_date ' +
                ',tx_id ' +
                ')';
            sql2 += 'values ( ?,?,?,?,?,?, ?, ?, 1,?,?,?)';
            params = [store_id, purchsSn, totQty,totAmt,details,sDate,sal_tax_price,sal_tax_type,pDate,tDate,tx_id];
        }else{
            sql2 = 'insert into store_sale (' +
                'store_id ' +
                ',purchsSn ' +
                ',totQty ' +
                ',totAmt ' +
                ',details ' +
                ',saleDatm ' +
                ',sal_tax_price' +
                ',sal_tax_type ' +
                ',sal_tax_yn ' +
                ')';
            sql2 += 'values ( ?,?,?,?,?,?,0,0,0)';
            params = [store_id, purchsSn, totQty,totAmt,details,sDate];
        }
    }

    console.log("sql2 확인 : "+sql2)

    posdb.query(sql2 ,params, function (err, rows) {
        posdb.release
        if(err){
            console.log(err)
        }else{
            res.send({result: "success" ,purchsSn :purchsSn});
        }
    })


})


module.exports = router;
