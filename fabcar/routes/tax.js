var express = require('express');
var router = express.Router();
var session = require('express-session');
var user_id = null;

/* GET home page. */
router.get('/txoffice', function(req, res, next) {
    res.render('tax', { title: 'txoffice' });
});
router.get('/txMake', function(req, res, next) {
    user_id =  req.session.user_id
    if(user_id != null){
        res.render('txMake', { title: 'txMake' ,user_id});
    }else{
        res.render('login', { title: 'login' ,err : '2' });
    }
});

router.post('/addtx', function(req, res, next){
    console.log(req.body);

    'use strict';
    /*
    * Copyright IBM Corp All Rights Reserved
    *
    * SPDX-License-Identifier: Apache-2.0
    */
    /*
     * Chaincode Invoke
     */

    var Fabric_Client = require('fabric-client');
    var Fabric_CA_Client = require('fabric-ca-client');
    var path = require('path');
    var util = require('util');
    var os = require('os');
//
    var fabric_client = new Fabric_Client();

// setup the fabric network
    var channel = fabric_client.newChannel('cr-taxrefund');
    var peer = fabric_client.newPeer('grpc://1ec2-13-209-181-30.ap-northeast-2.compute.amazonaws.com:7051');
    channel.addPeer(peer);
    var order = fabric_client.newOrderer('grpc://www.chainsearch.info:7050')
    channel.addOrderer(order);

//
    var member_user = null;
    var store_path = path.join(__dirname, 'hfc-key-store');
    console.log('Store path:'+store_path);
    var fabric_ca_client = null;
    var tx_id = null;
    var request = {};
    var crypto_suite = null;

//User ID
    var client_id = req.session.user_id;
    var store_id = req.body.store_id;

//amount for tax refund
    var amount = req.body.amount;

    var currentDate= new Date();
    var datetime = currentDate.getFullYear()+"-"
        +currentDate.getUTCMonth()+"-"+
        currentDate.getDate()+" "+
        currentDate.getHours()+":"+
        currentDate.getMinutes()+":"+
        currentDate.getSeconds();

// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
    Fabric_Client.newDefaultKeyValueStore({ path: store_path
    }).then((state_store) => {
        // assign the store to the fabric client
        fabric_client.setStateStore(state_store);
        crypto_suite = Fabric_Client.newCryptoSuite();
        // use the same location for the state store (where the users' certificate are kept)
        // and the crypto store (where the users' keys are kept)
        var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
        crypto_suite.setCryptoKeyStore(crypto_store);
        fabric_client.setCryptoSuite(crypto_suite);
        fabric_ca_client = new Fabric_CA_Client('http://192.168.0.36:7054', null , '', crypto_suite);
        // get the enrolled user from persistence, this user will sign all requests
        return fabric_client.getUserContext(client_id, true);
    }).then((user_from_store) => {
        if (user_from_store && user_from_store.isEnrolled()) {
            console.log('1. Successfully loaded "client" user - {client_id: '+client_id+'} from persistence');
        } else {
            throw new Error('Failed to get "client user" - {client_id: ' +client_id+'} .... run registerUser.js');
        }


        return fabric_client.getUserContext('admin', true);
    }).then((admin_from_store) => {
        if (admin_from_store && admin_from_store.isEnrolled()) {
            console.log('2. Successfully loaded {admin} from persistence - in order to access CA Server via admin certificate');
        } else {
            throw new Error('Failed to get "admin".... run enrollAdmin.js');
        }
        // get a transaction id object based on the current user assigned to fabric client

        return fabric_ca_client.newIdentityService().getOne(store_id, admin_from_store);
    }).then((store_from_server) => {

        // next we need to enroll the user with CA server
        if (store_from_server.result.type === 'Client.store'){
            member_user = store_from_server;
        }else{
            throw new Error('Failed to get "Client.store" user.... check your store_id again');
        }
        console.log('3. Successfully loaded "Client.store" user membership info {store_id: '+store_id+', type:' + store_from_server.result.type+'}');

        tx_id = fabric_client.newTransactionID();
        console.log("4. Assigning transaction_id: ", tx_id.getTransactionID());

        // get a transaction id object based on the current user assigned to fabric client
        tx_id = fabric_client.newTransactionID();

        // console.log("getUserContext: "+ JSON.parse(member_user, function (k, value) {
        //     if (k==='signingIdentity'){
        //         console.log("SigningIdentity: "+value)
        //     }
        // }));

        // createCar chaincode function - requires 6 args, ex: args: ['tx_id', 'client_id', 'store_id','5000','2018-05-26 17:40:33','0],
        // must send the proposal to endorsing peers
        request = {
            //targets: let default to the peer assigned to the client
            chaincodeId: 'cc_taxrefund',
            chaincodeVersion: 'v1.0',
            fcn: 'invokePending',
            args: [
                {
                    header: [
                        {
                            reference_hash: "1",
                            tx_status: "2",
                            request_type: "3",
                            refund_type: "4",
                            cur_tx_id: "5",
                            prev_tx_id: "6"
                        }
                    ]
                },
                {
                    body: {
                        customer_id: 1,
                        taxRefunder_id: 1,
                        store_id: 1,
                        tx_created: 1,
                        purchsSn: 1,
                        saleDatm: 1,
                        totQty: 1,
                        totAmt: 1,
                        totRefund: 1,
                        totVat: 1,
                        totIct: 1,
                        totEdut: 1,
                        totStr: 1,
                        details: [
                            {
                                prodNm: 2,
                                IneNo: 2,
                                prdCode: 2,
                                indQty: 2,
                                indPrice: 2,
                                salePrice: 2,
                                indVat: 2,
                                indIct: 2,
                                indEdut: 2,
                                indStr: 2
                            }
                        ]
                    }
                }
            ],
            chainId: 'cr-taxrefund',
            txId: tx_id
        };

        console.log("5. Send Transaction Request - ", JSON.stringify(request));
        // send the transaction proposal to the peers
        return channel.sendTransactionProposal(request);
    }).then((results) => {
        var proposalResponses = results[0];
        var proposal = results[1];
        let isProposalGood = false;
        if (proposalResponses && proposalResponses[0].response &&
            proposalResponses[0].response.status === 200) {
            isProposalGood = true;
            // console.log('Transaction proposal was good');
        } else {
            // console.error('Transaction proposal was bad');
        }
        if (isProposalGood) {
            console.log(util.format(
                '6. Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s"',
                proposalResponses[0].response.status, proposalResponses[0].response.message));

            // build up the request for the orderer to have the transaction committed
            var request = {
                proposalResponses: proposalResponses,
                proposal: proposal
            };

            // set the transaction listener and set a timeout of 30 sec
            // if the transaction did not get committed within the timeout period,
            // report a TIMEOUT status
            var transaction_id_string = tx_id.getTransactionID(); //Get the transaction ID string to be used by the event processing
            var promises = [];

            var sendPromise = channel.sendTransaction(request);
            promises.push(sendPromise); //we want the send transaction first, so that we know where to check status

            // get an eventhub once the fabric client has a user assigned. The user
            // is required bacause the event registration must be signed
            let event_hub = fabric_client.newEventHub();
            event_hub.setPeerAddr('grpc://www.chainsearch.info:7053');

            // using resolve the promise so that result status may be processed
            // under the then clause rather than having the catch clause process
            // the status
            let txPromise = new Promise((resolve, reject) => {
                let handle = setTimeout(() => {
                    event_hub.disconnect();
                    resolve({event_status : 'TIMEOUT'}); //we could use reject(new Error('Trnasaction did not complete within 30 seconds'));
                }, 3000);
                event_hub.connect();
                event_hub.registerTxEvent(transaction_id_string, (tx, code) => {
                    // this is the callback for transaction event status
                    // first some clean up of event listener
                    clearTimeout(handle);
                    event_hub.unregisterTxEvent(transaction_id_string);
                    event_hub.disconnect();

                    // now let the application know what happened
                    var return_status = {event_status : code, tx_id : transaction_id_string};
                    if (code !== 'VALID') {
                        console.error('The transaction was invalid, code = ' + code);
                        resolve(return_status); // we could use reject(new Error('Problem with the tranaction, event status ::'+code));
                    } else {
                        console.log('7. The transaction has been committed on peer ' + event_hub._ep._endpoint.addr);
                        resolve(return_status);
                    }
                }, (err) => {
                    //this is the callback if something goes wrong with the event registration or processing
                    reject(new Error('There was a problem with the eventhub ::'+err));
                });
            });
            promises.push(txPromise);

            return Promise.all(promises);
        } else {
            console.error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
            throw new Error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
        }
    }).then((results) => {
        console.log('8. Send transaction completed: '+JSON.stringify(results));
        // check the results in the order the promises were added to the promise all list
        if (results && results[0] && results[0].status === 'SUCCESS') {
            console.log('9. Successfully sent transaction to the orderer.');
            if(results && results[1] && results[1].event_status === 'VALID') {
                console.log('10. Successfully committed the change to the ledger by the peer');
            } else {
                console.log('Transaction failed to be committed to the ledger due to ::'+results[1].event_status);
            }
        } else {
            console.error('Failed to order the transaction. Error code: ' + response.status);
        }


    }).catch((err) => {
        console.error('Failed to invoke successfully :: ' + err);
    });

    res.redirect('/main');
});


router.post('/txList', function(req, res, next){
    console.log("트랜잭션 리스트 시작")
    var user_id = req.session.user_id;
    'use strict';
    /*
    * Copyright IBM Corp All Rights Reserved
    *
    * SPDX-License-Identifier: Apache-2.0
    */
    /*
     * Chaincode query
     */

    var Fabric_Client = require('fabric-client');
    var Fabric_CA_Client = require('fabric-ca-client');
    var path = require('path');
    var util = require('util');
    var os = require('os');

//
    var fabric_client = new Fabric_Client();

// setup the fabric network
    var channel = fabric_client.newChannel('cr-taxrefund');
    var peer = fabric_client.newPeer('grpc://ec2-13-209-181-30.ap-northeast-2.compute.amazonaws.com:7051');
    channel.addPeer(peer);

//
    var member_user = null;
    var store_path = path.join(__dirname, 'hfc-key-store');
    console.log('Store path:'+store_path);
    var tx_id = null;
// var user_id = 'korea_tax_office';
    var role_type=null;

    var fabric_ca_client = null;
    var crypto_store = null;
    var request = {};
    var errcode =null;
// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
    Fabric_Client.newDefaultKeyValueStore({ path: store_path
    }).then((state_store) => {
        // assign the store to the fabric client
        fabric_client.setStateStore(state_store);
        var crypto_suite = Fabric_Client.newCryptoSuite();
        console.log('cryto_suite1'+JSON.stringify(state_store));
        // use the same location for the state store (where the users' certificate are kept)
        // and the crypto store (where the users' keys are kept)
        crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
        console.log('<---------------------------------------------------------------------------------------------------->');
        console.log('cryto_store1'+JSON.stringify(crypto_store));
        crypto_suite.setCryptoKeyStore(crypto_store);
        console.log('<---------------------------------------------------------------------------------------------------->');
        console.log('crypto_suite2'+JSON.stringify(crypto_suite));
        fabric_client.setCryptoSuite(crypto_suite);
        console.log('<---------------------------------------------------------------------------------------------------->');
        console.log('cryto_suite2'+JSON.stringify(crypto_suite));
        fabric_ca_client = new Fabric_CA_Client('http://192.168.0.36:7054', null , '', crypto_suite);
        // get the enrolled user from persistence, this user will sign all requests
        return fabric_client.getUserContext('admin', true);
    }).then((user_from_store) => {
        if (user_from_store && user_from_store.isEnrolled()) {
            console.log('1. Successfully loaded {admin} from persistence - in order to access CA Server via admin certificate');
            member_user = user_from_store;
            console.log('cryto_suite2'+(user_from_store));
        } else {
            throw new Error('Failed to get '+user_id+'.... run registerUser.js');
        }
        // queryCar chaincode function - requires 1 argument, ex: args: ['CAR4'],
        // queryAllCars chaincode function - requires no arguments , ex: args: [''],
        return fabric_ca_client.newIdentityService().getOne(user_id, user_from_store);
    }).then((identity) => {
        // next we need to enroll the user with CA server
        role_type = identity.result.type;
        console.log('2. Successfully loaded user membership info from CA Server - {user_id: '+user_id+', role: ' + role_type+'}');

        // if you want to scan data by transaction id, this code will be executed
        if (tx_id!==null){
            request = {
                //targets : --- letting this default to the peers assigned to the channel
                chaincodeId: 'cc_taxrefund',
                fcn: 'getQueryResultForSpecificKey',
                args: [role_type, tx_id]
            };
            // send the query proposal to the peer
            return channel.queryByChaincode(request);
        }

        if (role_type === 'Client.customer') {
            request = {
                //targets : --- letting this default to the peers assigned to the channel
                chaincodeId: 'cc_taxrefund',
                // chaincodeVersion: 'v1.1',
                fcn: 'getQueryResultForSpecificKey',
                args: [role_type, user_id]
            };
        } else if (role_type === 'Client.store'){
            request = {
                //targets : --- letting this default to the peers assigned to the channel
                chaincodeId: 'cc_taxrefund',
                // chaincodeVersion: 'v1.1',
                fcn: 'getQueryResultForSpecificKey',
                args: [role_type, user_id]
            };
        } else if(role_type ==='Client.taxoffice'){
            request = {
                //targets : --- letting this default to the peers assigned to the channel
                chaincodeId: 'cc_taxrefund',
                // chaincodeVersion: 'v1.0',
                fcn: 'queryAllTransactions',
                args: ['Client.taxoffice']
            };
        } else {
            throw new Error('Failed to get user.... run registerUser.js');
        }

        console.log('3. Request Data by "role": '+role_type+' ------> ' + request);

        // send the query proposal to the peer
        return channel.queryByChaincode(request);
    }).then((query_responses) => {
        console.log("4. Query has completed, checking results");
        // query_responses could have more than one  results if there multiple peers were used as targets
        if (query_responses && query_responses.length == 1) {
            if (query_responses[0] instanceof Error) {
                console.error("error from query = ", query_responses[0]);
            } else {
                console.log("5. Response is ", query_responses[0].toString());
                console.log("확인 ", query_responses[0]);
                var jsontext = query_responses[0].toString();
                var contact = JSON.parse(jsontext);
                res.send({result: contact});
            }
        } else {
            console.log("No payloads were returned from query");
        }
    }).catch((err) => {
        console.error('Failed to query successfully :: ' + err);
    });



});

module.exports = router;
