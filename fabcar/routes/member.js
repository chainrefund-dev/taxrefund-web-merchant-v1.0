var express = require('express');
var router = express.Router();
var session = require('express-session');
var app = express();
app.use(session({
    secret: '12312dajfj23rj2po4$#%@#',
    resave: false,
    saveUninitialized: true
}));



/* GET home page. */
router.get('/login', function(req, res, next) {
    res.render('login', { title: 'login' ,err :'0'});
});
router.post('/login', function(req, res, next){
    console.log(req.body);

    var id = req.body.user_id;
    var pwd = req.body.user_pwd;
    'use strict';
    /*
    * Copyright IBM Corp All Rights Reserved
    *
    * SPDX-License-Identifier: Apache-2.0
    */
    /*
     * Chaincode query
     */

    var Fabric_Client = require('fabric-client');
    var Fabric_CA_Client = require('fabric-ca-client');
    var path = require('path');
    var util = require('util');
    var os = require('os');

//
    var fabric_client = new Fabric_Client();

// setup the fabric network
    var channel = fabric_client.newChannel('mychannel');
    var peer = fabric_client.newPeer('grpc://192.168.0.36:7053');
    channel.addPeer(peer);

//
    var member_user = null;
    var store_path = path.join(__dirname, 'hfc-key-store');
    console.log('Store path:'+store_path);
    var tx_id = null;
// var user_id = 'korea_tax_office';
    var role_type=null;

    var fabric_ca_client = null;
    var crypto_store = null;
    var request = {};
    var errcode =null;
// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
    Fabric_Client.newDefaultKeyValueStore({ path: store_path
    }).then((state_store) => {
        // assign the store to the fabric client
        fabric_client.setStateStore(state_store);
        var crypto_suite = Fabric_Client.newCryptoSuite();
        console.log('cryto_suite1'+JSON.stringify(state_store));
        // use the same location for the state store (where the users' certificate are kept)
        // and the crypto store (where the users' keys are kept)
        crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
        console.log('<---------------------------------------------------------------------------------------------------->');
        console.log('cryto_store1'+JSON.stringify(crypto_store));
        crypto_suite.setCryptoKeyStore(crypto_store);
        console.log('<---------------------------------------------------------------------------------------------------->');
        console.log('crypto_suite2'+JSON.stringify(crypto_suite));
        fabric_client.setCryptoSuite(crypto_suite);
        console.log('<---------------------------------------------------------------------------------------------------->');
        console.log('cryto_suite2'+JSON.stringify(crypto_suite));
        fabric_ca_client = new Fabric_CA_Client('http://192.168.0.36:7054', null , '', crypto_suite);
        // get the enrolled user from persistence, this user will sign all requests
        return fabric_client.getUserContext('admin', true);
    }).then((user_from_store) => {
        if (user_from_store && user_from_store.isEnrolled()) {
            console.log('1. Successfully loaded {admin} from persistence - in order to access CA Server via admin certificate');
            member_user = user_from_store;
            console.log('cryto_suite2'+(user_from_store));
        } else {
            throw new Error('Failed to get '+user_id+'.... run registerUser.js');
        }
        // queryCar chaincode function - requires 1 argument, ex: args: ['CAR4'],
        // queryAllCars chaincode function - requires no arguments , ex: args: [''],
        return fabric_ca_client.newIdentityService().getOne(id, user_from_store);
    }).then((identity) => {
        // next we need to enroll the user with CA server
        role_type = identity.result.type;
        console.log('2. Successfully loaded user membership info from CA Server - {user_id: '+id+', role: ' + role_type+'}');
        req.session.user_id = id
        req.session.role_type = role_type
        res.redirect('/main');
    }).catch((err) => {
        errcode = '1'
        console.error('Failed to query successfully :: ' + err);
        res.render('login', { err: '1',title: 'login' });
    });

});

router.get('/create', function(req, res, next) {
    res.render('mcreate', { title: 'MemberCreate' });
});

router.post('/cmember', function(req, res, next){
    console.log(req.body);

    /*
     * Register and Enroll a user
     */

    var Fabric_Client = require('fabric-client');
    var Fabric_CA_Client = require('fabric-ca-client');

    var path = require('path');
    var util = require('util');
    var os = require('os');
//
    var fabric_client = new Fabric_Client();
    var fabric_ca_client = null;
    var admin_user = null;
    var member_user = null;
    var store_path = path.join(__dirname, 'hfc-key-store');
    console.log(' Store path:'+store_path);
    var user_id = req.body.user_id;
    var user_role = req.body.user_role;
    var password = req.body.user_pwd;
    var user_affiliation='org2.department1';
// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
    Fabric_Client.newDefaultKeyValueStore({ path: store_path
    }).then((state_store) => {
        console.log('시작');
        // assign the store to the fabric client
        fabric_client.setStateStore(state_store);
        var crypto_suite = Fabric_Client.newCryptoSuite();
        // use the same location for the state store (where the users' certificate are kept)
        // and the crypto store (where the users' keys are kept)
        var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
        crypto_suite.setCryptoKeyStore(crypto_store);
        fabric_client.setCryptoSuite(crypto_suite);

        var	tlsOptions = {
            trustedRoots: [],
            verify: false
        };
        // be sure to change the http to https when the CA is running TLS enabled
        fabric_ca_client = new Fabric_CA_Client('http://192.168.0.36:7054', null , '', crypto_suite);

        // first check to see if the admin is already enrolled
        return fabric_client.getUserContext('admin', true);
    }).then((user_from_store) => {
        if (user_from_store && user_from_store.isEnrolled()) {
            console.log('1. Successfully loaded {admin} from persistence - in order to access CA Server via admin certificate');
            admin_user = user_from_store;
        } else {
            throw new Error('Failed to get admin.... run enrollAdmin.js');
        }


        // at this point we should have the admin user
        // first need to register the user with the CA server

        return fabric_ca_client.register({enrollmentID: user_id, affiliation: user_affiliation, role: user_role, enrollmentSecret: password}, admin_user);
    }).then((secret) => {
        // next we need to enroll the user with CA server
        console.log('2. Successfully registered {user_id: '+user_id+', affiliation: '+user_affiliation+', role: '+user_role+', secret:' + secret+'}');

        return fabric_ca_client.enroll({enrollmentID: user_id, enrollmentSecret: secret});
    }).then((enrollment) => {
        console.log('3. Successfully enrolled member user {id: ' +user_id+ '}');
        return fabric_client.createUser(
            {username: user_id,
                role: user_role,
                affiliation: user_affiliation,
                mspid: 'TaxRefundSysMSP',
                cryptoContent: { privateKeyPEM: enrollment.key.toBytes(), signedCertPEM: enrollment.certificate }
            });
    }).then((user) => {
        console.log('4. ' + user_id + ' was successfully registered and enrolled and is ready to interact with the fabric network' );
        member_user = user;
        user.setRoles(user_role);
        user.setAffiliation(user_affiliation)
        return fabric_client.setUserContext(member_user);
    }).then(()=> {
        console.log('4. ' + user_id + '`s certificate was successfully set up with roles and affiliation' );

    }).catch((err) => {
        console.error('Failed to register: ' + err);
        if(err.toString().indexOf('Authorization') > -1) {
            console.error('Authorization failures may be caused by having admin credentials from a previous CA instance.\n' +
                'Try again after deleting the contents of the store directory '+store_path);
        }
    });


    res.render('login', { title: 'login' ,err : '0' });
});

router.get('/member', function(req, res, next) {
    res.render('member', { title: 'memberList' });
});
router.post('/memberList', function(req, res, next) {
    var user_id = req.session.user_id
    console.log(req.body);

    var pwd = req.body.user_pwd;
    'use strict';
    /*
    * Copyright IBM Corp All Rights Reserved
    *
    * SPDX-License-Identifier: Apache-2.0
    */
    /*
     * Chaincode query
     */

    var Fabric_Client = require('fabric-client');
    var Fabric_CA_Client = require('fabric-ca-client');
    var path = require('path');
    var util = require('util');
    var os = require('os');

//
    var fabric_client = new Fabric_Client();

// setup the fabric network
    var channel = fabric_client.newChannel('mychannel');
    var peer = fabric_client.newPeer('grpc://192.168.0.36:7053');
    channel.addPeer(peer);

//
    var member_user = null;
    var store_path = path.join(__dirname, 'hfc-key-store');
    console.log('Store path:'+store_path);
    var tx_id = null;
// var user_id = 'korea_tax_office';
    var role_type=null;

    var fabric_ca_client = null;
    var crypto_store = null;
    var request = {};
    var errcode =null;
// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
    Fabric_Client.newDefaultKeyValueStore({ path: store_path
    }).then((state_store) => {
        // assign the store to the fabric client
        fabric_client.setStateStore(state_store);
        var crypto_suite = Fabric_Client.newCryptoSuite();
        console.log('cryto_suite1'+JSON.stringify(state_store));
        // use the same location for the state store (where the users' certificate are kept)
        // and the crypto store (where the users' keys are kept)
        crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
        console.log('<---------------------------------------------------------------------------------------------------->');
        console.log('cryto_store1'+JSON.stringify(crypto_store));
        crypto_suite.setCryptoKeyStore(crypto_store);
        console.log('<---------------------------------------------------------------------------------------------------->');
        console.log('crypto_suite2'+JSON.stringify(crypto_suite));
        fabric_client.setCryptoSuite(crypto_suite);
        console.log('<---------------------------------------------------------------------------------------------------->');
        console.log('cryto_suite2'+JSON.stringify(crypto_suite));
        fabric_ca_client = new Fabric_CA_Client('http://192.168.0.36:7054', null , '', crypto_suite);
        // get the enrolled user from persistence, this user will sign all requests
        return fabric_client.getUserContext(user_id, true);
    }).then((user_from_store) => {
        if (user_from_store && user_from_store.isEnrolled()) {
            console.log('1. Successfully loaded {admin} from persistence - in order to access CA Server via admin certificate');
            member_user = user_from_store;
            console.log('cryto_suite2'+(user_from_store));
        } else {
            throw new Error('Failed to get '+user_id+'.... run registerUser.js');
        }
        // queryCar chaincode function - requires 1 argument, ex: args: ['CAR4'],
        // queryAllCars chaincode function - requires no arguments , ex: args: [''],
        return fabric_ca_client.newIdentityService().getAll(user_from_store);
    }).then((identity) => {
        // next we need to enroll the user with CA server
        console.log("2. Response is ", identity.result.identities);
        console.log("----------------------------------------------");
        console.log("3. Response is ", identity.result.identities[1].attrs);
        res.send({result: identity.result.identities});

    }).catch((err) => {
        errcode = '1'
        console.error('Failed to query successfully :: ' + err);
        res.render('login', { err: '1',title: 'login' });
    });
});
router.get('/logout', function(req, res, next) {
    req.session.destroy();  // 세션 삭제
    res.render('login', { title: 'login' ,err : '0' });
});

module.exports = router;
