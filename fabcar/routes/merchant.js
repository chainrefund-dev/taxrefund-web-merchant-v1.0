var express = require('express');
var router = express.Router();
var session = require('express-session');
var user_id = null;
var role_type = null;
//db설정
var db = require('./sql.js');
var posdb= db.posConnect();
var sysdb = db.sysConnect();
//db설정 end

    /* GET home page. */
router.get('/', function(req, res, next) {

     user_id =  req.session.user_id
     role_type =  req.session.role_type
    /*if(user_id != null){
        res.render('main', { title: 'main' ,user_id,role_type});
    }else{
        res.render('login', { title: 'login' ,err : '2' });
    }*/
    res.render('merchant/merchant', { title: 'merchant'});


});
router.post('/merchantList', function(req, res, next) {

    var page = req.body.page;
    var startRow = (page-1) * 15;
    console.log(page);
    var params =[startRow, 15];

    var sql = 'select *, date_format(registered_date, "%Y-%m-%d %T") as reg_date , date_format(ca_registered_date, "%Y-%m-%d %T") as ca_reg_date   from store_info_private order by row_id desc limit ? ,? ';
    var sql2 = 'select * from store_info_private ';
    posdb.query(sql2, function (err, rows) {
        posdb.release
        if(err){
            console.log(err)
        } else {
            console.log("sql2처리"+rows.length)
            var tot = rows.length;
            posdb.query(sql,params, function (err, rows) {
                posdb.release
                if(err){
                    console.log(err)
                } else {
                    console.log("sql처리완료"+JSON.stringify(rows))
                    res.send({rows: rows , nowrow:tot});
                }
            });
        }
    });

});

router.get('/merchant_detail', function(req, res, next) {
    var no = req.query.no
    console.log("no: "+no)
    var sql = 'select *,date_format(registered_date, "%Y-%m-%d %T") as reg_date, date_format(ca_registered_date, "%Y-%m-%d %T") as ca_reg_date from store_info_private where row_id = ? ';


    posdb.query(sql,no, function (err, rows, fields) {
        posdb.release
        if(err){
            console.log(err)
        } else {
            console.log(JSON.stringify(rows))
            res.render('merchant/merchant_detail', { rows: rows});
        }
    });


});


router.get('/merchant_add', function(req, res, next) {
    res.render('merchant/merchant_add', { title: 'merchant_add'});

});
router.post('/insertMerchant', function(req, res, next) {
    console.log(req.body);
    var store_id = req.body.store_id;
    var store_name = req.body.store_name;
    var biz_num = req.body.biz_num;
    var rep_name = req.body.rep_name;
    var phone = req.body.phone;
    var address = req.body.address;
    var assign_num = req.body.assign_num;
    var add_ca_date = req.body.add_ca_date;
    var params =[store_id,store_name,biz_num,rep_name,phone,address,assign_num,add_ca_date];

    var sql = 'insert into store_info_private (store_id ,store_name ,biz_num ,rep_name ,phone ,address ,registered_date ,assign_num,ca_registered_date)';
    sql += 'values ( ? ,? ,? ,? ,? ,? , date_format(now(), "%Y-%m-%d %T") ,?, ?  )';



    posdb.query(sql,params, function (err, rows) {
        posdb.release
        if(err){
            console.log(err)
        } else {
            console.log("성공")
            res.send({result: "success"});
        }


    });
});
router.post('/deleteMerchant', function(req, res, next) {
    console.log(req.param('no'));

    var sql = 'delete from store_info_private where row_id = ?';


    posdb.query(sql,req.param('no'), function (err, rows) {
        posdb.release
        if(err){
            console.log(err)
        } else {

            res.send({result: "success"});
        }


    });
});


module.exports = router;
