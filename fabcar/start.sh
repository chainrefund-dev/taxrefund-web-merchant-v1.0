#!/bin/bash
DOCKER_CONTAINER_NAME="store-fabric-client"
DOCKER_CA_REGISTRY_IMAGE_NAME="store-fabric-client/nodejs"
DOCKER_CA_REGISTRY_IMAGE_TAG="v1.0.0"
COMPOSE_FILE=docker-compose.yaml
echo "start CA Registry Server"


function restartContainers() {
  docker rm -f $(docker ps -aqf name="${DOCKER_CONTAINER_NAME}")
  docker-compose -f ${COMPOSE_FILE} up -d 2>&1
}

function rebuildDocker() {
    echo "Stop and Remove current Store-Fabric-Client docker container..."
    docker rm -f $(docker ps -aqf name="${DOCKER_CONTAINER_NAME}")

    if [[ "$(docker images -q ${DOCKER_CA_REGISTRY_IMAGE_NAME}:${DOCKER_CA_REGISTRY_IMAGE_TAG} 2> /dev/null)" != "" ]]; then
        echo "Remove Store-Fabric-Client docker IMAGE!!.."
        docker rmi -f ${DOCKER_CA_REGISTRY_IMAGE_NAME}:${DOCKER_CA_REGISTRY_IMAGE_TAG}
    fi

    echo "Build Store-Fabric-Client image.."
    docker build -t ${DOCKER_CA_REGISTRY_IMAGE_NAME}:${DOCKER_CA_REGISTRY_IMAGE_TAG} .
    docker-compose -f ${COMPOSE_FILE} up -d 2>&1
}


function askProceed () {
  read -p "Remove Store-Fabric-Client images and container to rebuild those items(Y) or just restart container(N) ? [Y/N] " ans
  case "$ans" in
    y|Y|"" )
      echo "Remove and rebuild docker container of Store-Fabric-Client"
      rebuildDocker
    ;;
    n|N )
      echo "Restart docker container of Store-Fabric-Client..."
      restartContainers
    ;;
    * )
      echo "invalid response"
      askProceed
    ;;
  esac
}


askProceed




